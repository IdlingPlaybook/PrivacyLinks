# PrivacyLinks

## Contents
  - [Private Frond Ends](#private-frond-ends)
  - ["Private" E-Mail](#private-e-mail)
  - [Temporary E-Mail Services](#temporary-email-services)
  - [News Sites](#news-sites)
  - [Search Engines](#search-engines)
  - [Onion Search Engines](#onion-search-engines)
  - [Onion Indexes](#onion-indexes)
  - [Onion Forums](#onion-forums)
  - [Privacy Oriented Websites](#privacy-oriented-websites)
  - [Other Privacy Respecting Alternatives](#other-privacy-respecting-alternatives)
  - [Torrent Collections](#torrent-collections)
  - [Other Collections](#other-collections)
  - [Other Interesting Websites](#other-interesting-websites)
  - [External Lists](#external-lists)

## Private Frond Ends

| Original Site | Private Site | Onion  | Source |
| ------------- | ------------ | ------ | ------ |
| [YouTube](https://youtube.com) | [Invidious](https://yewtu.be) | [Onion](http://c7hqkpkpemu6e7emz5b4vyz7idjgdvgaaa3dyimmeojqbgpea3xqjoid.onion) | [Source](https://invidious.io) |
| [YouTube](https://youtube.com) | [Piped](https://piped.kavin.rocks) | | [Source](https://github.com/TeamPiped/Piped) |
| [Twitter](https://twitter.com) | [Nitter](https://nitter.net) | [Onion](http://nitter7bryz3jv7e3uekphigvmoyoem4al3fynerxkj22dmoxoq553qd.onion) | [Source](https://github.com/zedeus/nitter) |
| [Reddit](https://reddit.com) | [Teddit](https://teddit.net) | [Onion](http://qtpvyiaqhmwccxwzsqubd23xhmmrt75tdyw35kp43w4hvamsgl3x27ad.onion) | [Source](https://codeberg.org/teddit/teddit) |
| [Reddit](https://reddit.com) | [libreddit](https://libredd.it) | [Onion](http://spjmllawtheisznfs7uryhxumin26ssv2draj7oope3ok3wuhy43eoyd.onion) | [Source](https://github.com/spikecodes/libreddit) |
| [Instagram](https://instagram.com) | [Bibliogram](https://bibliogram.art) | [Onion](http://rlp5gt4d7dtkok3yaogocbcvrs2tdligjrxipsamztjq4wwpxzjeuxqd.onion) | [Source](https://sr.ht/~cadence/bibliogram) |
| [Wikipedia](https://wikipedia.org) | [Wikiless](https://wikiless.org) | | [Source](https://codeberg.org/orenom/wikiless) |
| [Google Translate](https://translate.google.com) | [SimplyTranslate](https://simplytranslate.org) | [Onion](http://fmgp3rg56ng6mtb5gvu5hgzwwdyzgkmnanettwnmbnueues7ndw2fkyd.onion) | [Source](https://sr.ht/~metalune/SimplyTranslate/sources)
| [Google Translate](https://translate.google.com) | [Lingva Translate](https://lingva.ml) | [Onion](http://beko4bipbbqvwjizoswa3gcjrj3fdgb6nqthv7mt2gcswd2nln45ooid.onion) | [Source](https://github.com/TheDavidDelta/lingva-translate) |
| [Imgur](https://imgur.com) | [imgin](https://imgin.voidnet.tech) | | [Source](https://git.voidnet.tech/kev/imgin)
| [Imgur](https://imgur.com) | Rimgu | | [Source](https://codeberg.org/3np/rimgu) |

## "Private" E-Mail

| Site | Onion |
| ---- | ----- |
| [Protonmail](https://protonmail.com) | [Onion](https://protonmailrmez3lotccipshtkleegetolb73fuirgj7r4o4vfu7ozyd.onion) |
| [Tutanota](https://tutanota.com) | |
| [Cock.li](https://cock.li) | [Onion](http://rurcblzhmdk22kttfkel2zduhyu3r6to7knyc7wiorzrx5gw4c3lftad.onion) |
| [Onion Mail](https://onionmail.org) | [Onion](http://pflujznptk5lmuf6xwadfqy6nffykdvahfbljh7liljailjbxrgvhfid.onion) |
| Dark Net Mail Exchange | [Onion](http://hxuzjtocnzvv5g2rtg2bhwkcbupmk7rclb6lly3fo4tvqkk5oyrv3nid.onion) |
| Elude | [Onion](http://eludemailxhnqzfmxehy3bk5guyhlxbunfyhkcksv4gvx6d3wcf6smad.onion) |

## Temporary Email Services

| Site | Onion | About |
| ---- | ----- | ----- |
| [Guerrilla Mail](https://guerrillamail.com) | [Onion](http://csmailsbb4arfihowfmrpotsceq6pe66t43q2els2nib2qdoj745j5ad.onion) | [Open Source](https://github.com/flashmob/go-guerrilla)
| Alt Address | [Onion](http://tp7mtouwvggdlm73vimqkuq7727a4ebrv4vf4cnk6lfg4fatxa6p2ryd.onion) | disposable emails for 3 days |
| [Gmailnator](https://www.gmailnator.com) | | Disposable Gmail Adresses |
| [temp-mail](https://temp-mail.io) | | |

## News Sites

| Site | Onion |
| ---- | ----- |
| [ProPublica](https://www.propublica.org) | [Onion](http://p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion) |
| [Darknetlive](https://darknetlive.com) | [Onion](http://darkzzx4avcsuofgfez5zq75cqc4mprjvfqywo45dfcaxrwqg6qrlfid.onion) |

## Search Engines

| Original Site | Onion  | About |
| ------------- | ------ | ----- |
| [DuckDuckGo](https://duckduckgo.com) | [Onion](https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion) |
| [searX](https://searx.bar) | [Onion](http://yra4tke2pwcnatxjkufpw6kvebu3h3ti2jca2lcdpgx3mpwol326lzid.onion) |
| [Whoogle](https://www.whooglesearch.m) | [Onion](http://whoogledq5f5wly5p4i2ohnvjwlihnlg4oajjum2oeddfwqdwupbuhqd.onion) | [Get Google results without ads, tracking](https://github.com/benbusby/whoogle-search) |
| [Startpage](https://startpage.com) | | [Get Google Results privatly](https://www.startpage.com/en/about-us) |
| [Qwant](https://www.qwant.com) | | |
| [brave](https://search.brave.com) | |
| [metaGer](https://metager.org) | |

## Onion Search Engines

| Site | Onion | Info |
| ---- | ----- | ---- |
| Onion Search Engine | [Onion](http://zgphrnyp45suenks3jcscwvc5zllyk3vz4izzw67puwlzabw4wvwufid.onion) | |
| [Ahmia](https://ahmia.fi) | [Onion](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion) | May block sensitive content |
| torch | [Onion](http://xmh57jrknzkhv6y3ls3ubitzfqnkrwxhopf5aygthi7d6rplyvk3noyd.onion/cgi-bin/omega/omega) | |
| Phobos | [Onion](http://phobosxilamwcg75xt22id7aywkzol6q6rfl2flipcqoc4e4ahima5id.onion) | |

## Onion Indexes

| Site |
| ---- |
| [Deepl Links Dump](http://deepqelxz6iddqi5obzla2bbwh5ssyqqobxin27uzkr624wtubhto3ad.onion) |
| [OnionLinks](http://s4k4ceiapwwgcm3mkb6e4diqecpo7kvdnfr5gg7sph7jjppqkvwwqtyd.onion) |
| [The Hidden Wiki](http://zqktlwiuavvvqqt4ybvgvi7tyo4hjl5xgfuvpdf6otjiycgwqbym2qad.onion/wiki/index.php/Main_Page) |
| [Hidden Wiki Fresh](http://wikiw2godl6vm5amb4sij47rwynnrmqenwddykzt3fwpbx6p34sgb7yd.onion) |
| [DeepLink Onion Directory](http://deeeepv4bfndyatwkdzeciebqcwwlvgqa6mofdtsvwpon4elfut7lfqd.onion) |
| [Darknet Home](http://catalogpwwlccc5nyp3m3xng6pdx3rdcknul57x6raxwf4enpw3nymqd.onion) |
| [Raptor](http://raptora2y6r3bxmjcd3xglr3tcakc6ezq3omyzbnvwahhpi27l3w4yad.onion) |

## Onion Forums
| Site | Onion |
| ---- | ----- |
| dread | [Onion](http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion) |
| [8chan](https://8chan.moe) | [Onion](http://4usoivrpy52lmc4mgn2h34cmfiltslesthr56yttv2pxudd3dapqciyd.onion) |
| CryptBB | [Onion](http://cryptbbtg65gibadeeo2awe3j7s6evg7eklserehqr4w4e2bis5tebid.onion) |
| Carding World | [Onion](http://bestteermb42clir6ux7xm76d4jjodh3fpahjqgbddbmfrgp4skg2wqd.onion)
| [GreySec](https://greysec.net) | |
| [supray](https://forum.suprbay.org) | [Onion](http://suprbaydvdcaynfo4dgdzgxb4zuso7rftlil5yg5kqjefnw4wq4ulcad.onion) |

## Privacy Oriented Websites

| Site | Onion |
| ---- | ----- |
| [EFF](https://eff.org) | |
| [PrivacyTools](https://www.privacytools.io) | [Onion](http://privacy2zbidut4m4jyj3ksdqidzkw3uoip2vhvhbvwxbqux5xy5obyd.onion) |
| [Privacy Guides](https://privacyguides.org) | |
| [AlternativeTo](https://alternativeto.net) | |

## Other Privacy Respecting Alternatives
| Site | Onion | Usecase |
| ---- | ----- | ------- |
| TorPaste | [Onion](http://torpastezr7464pevuvdjisbvaf4yqi4n7sgz7lkwgqwxznwy5duj4ad.onion) | Onion pastebin alternative |
| [xBrowserSync](https://www.xbrowsersync.org) | | Sync browser bookmarks |

## Torrent Collections
Disclaimer: Some of these Sites may link to copyrighted material and/or malware. Please be aware.

| Site | Onion | About |
| ---- | ----- | ------- |
| [The Pirate Bay](https://thepiratebay.org) | [Onion](http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion) | Biggest torrent collection |
| [RARBG](https://rarbg.to) | | Torrent collection |
| [rutracker](https://rutracker.org) | | Russian torrent collection |
| [YTS](https://yts.mx) | | Movies |
| [EZTV](https://eztv.re) | | TV Shows |
| [Nyaa](https://nyaa.si) | | | Anime |
| [SubsPlease](https://subsplease.org) | | Anime |
| [Skidrow & Reloaded](https://skidrowreloaded.com) | | Games |
| [1337X](https://1337X.to) | | Applications |
| [Library Genesis](https://libgen.st) | | E-Books |

## Other Collections
Disclaimer: Some of these Sites may host copyrighted material. Please be aware.

| Site | Onion | About | Username:Password |
| ---- | ----- | ------- | ----------------- |
| [Z-Library](https://z-lib.org) | [Onion](http://zlibrary24tuxziyiyfr7zd46ytefdqbqd2axkmxm4o5374ptpc52fad.onion) | Books, Articles | 4frkxqpad0@email.edu.pl:4frkxqpad0@email.edu.pl |
| [PDF Drive](https://pdfdrive.com) | | Books |
| Just Another Library | [Onion](http://libraryfyuybp7oyidyya3ah5xvwgyx6weauoini7zyz555litmmumad.onion) | Books, Gallery, Courses, Audiobooks | |
| Imperial Library | [Onion](kx5thpx2olielkihfyo4jgjqfb7zx7wxr3sd4xzt26ochei4m6f7tayd.onion) | Books | |
|
| [awesome-piracy](https://github.com/Igglybuff/awesome-piracy) | | "A curated list of awesome warez and piracy links" | |

## Other Interesting Websites

| Site | Onion | About |
| ---- | ----- | ----- |
| [BlackHost](https://blackhost.xyz) | [Onion](http://piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion) | Chat, File Upload, Mailer, Pastebin, Program Download, Webmail |

## External Lists

| Site | About | Authors |
| ---- | ----- | ----- |
| [alternative-front-ends](https://github.com/mendel5/alternative-front-ends) | Overview of alternative open source front-ends for popular internet platforms | [mendel5](https://github.com/mendel5) |
| [awesome-piracy](https://github.com/Igglybuff/awesome-piracy) | A curated list of awesome warez and piracy links | [lggybuff](https://github.com/Igglybuff) |
| [Awesome-Selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted) | A list of Free Software network services and web applications which can be hosted on your own servers | [Authors](https://github.com/awesome-selfhosted/awesome-selfhosted/blob/master/AUTHORS.md) |
| [Awesome Privacy](https://git.nixnet.services/pluja/awesome-privacy) | A curated list of services and alternatives that respect your privacy | [pluja](https://github.com/pluja) |

